package com.turner.exercise.interfaces;

import java.io.IOException;

/**
 * A Deduplicator instance will remember the last content that it saw for each path and
 * tell whether each subsequent FileContent instance it sees is a duplicate or not for that path.
 */
public interface Deduplicator
{
	public boolean isDuplicate(FileContent fileContent) throws IOException;
}
